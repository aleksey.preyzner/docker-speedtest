ARG SPDTST_VERSION
FROM alpine:3.14
ENV VERSION=${SPDTST_VERSION:-1.0.0}
WORKDIR /speedtest
RUN apk add curl tar gzip bzip2 \
    && curl -sO https://install.speedtest.net/app/cli/ookla-speedtest-${VERSION}-x86_64-linux.tgz \
    && tar -xzvf ookla-speedtest-${VERSION}-x86_64-linux.tgz \
    && chmod +x speedtest
RUN ["/speedtest/speedtest", "--accept-license", "--accept-gdpr"]
ENTRYPOINT [ "/speedtest/speedtest"]
CMD ["-f", "json-pretty"]